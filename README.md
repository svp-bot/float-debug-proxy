float-debug-proxy
===

A SOCKS5 proxy server for testing environments.

This tool solves a very specific problem: when using
[float](https://git.autistici.org/ai3/float) test environments, we'd
like to use a browser to debug the services, but the browser needs its
own testing environment, using the DNS provided by the test
environment itself. At the same time, we would like to do this without
any changes to the user environment (such as manually creating entries
in /etc/hosts, for instance), because they pose an unreasonable burden
on the user.

One way to achieve this with low friction is to use a proxy running on
the test environment itself, so that by simply pointing the browser at
it would get you the "internal" view. SSH can easily do this by
running its own SOCKS proxy, for example, but then we run into another
issue: ssh uses the system resolver, and if we point
*/etc/resolv.conf* at the test environment authoritative DNS servers,
we break the test environment isolation and make it harder to run
environments with "real" domains (if there is any overlap with the CI
domains, for instance, Ansible will break). That's how we get to the
tool you're looking at right now: a simple SOCKS5 proxy, that can be
pointed at a specific DNS resolver, ignoring the system
/etc/resolv.conf. It will run on a test environment frontend node,
without any authentication (these are non-public hosts). You can then
run a test browser session with:

    $BROWSER --user-data-dir /tmp/test --proxy-server=socks5://$IP:9051

(with suitable values of $BROWSER and $IP).
