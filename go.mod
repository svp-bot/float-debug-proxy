module git.autistici.org/ai3/tools/float-debug-proxy

go 1.15

require (
	github.com/armon/go-socks5 v0.0.0-20160902184237-e75332964ef5
	github.com/miekg/dns v1.1.42
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
)
